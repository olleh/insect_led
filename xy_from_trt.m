## Copyright (C) 2016 Olle Håstad
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} xy_from_trt (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Olle Håstad <olle@weasel.skallbe.net>
## Created: 2016-07-16

function xy = xy_from_trt (trt_filename, integrationtime_ms)
nm = 300:750;
load 'data/cal_ava_cc_200.csv' cal_ava;
load 'data/sens.mat' sens;
counts_p_sec = read_trt_to_frame(trt_filename ,nm) ./ (integrationtime_ms / 1000);
spec_energy = counts_p_sec' .* cal_ava(:,2);
receptor_stim = receptor_stimulus_from_spectrum(spec_energy' ,sens(:,4:6));
xy= xy_from_receptor_stimulus(receptor_stim );
endfunction
