function receptorstim = calc_stim_from_led(l, w, sens)
%calc_stim_from_led Calculates the receptor stimulus from a led of given
%lambda max on a set om receptors

    led = repmat(normpdf(w,l,8)', 1, 6);
    stim = sens .* led;
    receptorstim = sum(stim,1);
end
