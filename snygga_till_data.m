%% Fix data from Chittka och CIE
 
% Read the files with honey bee sens
u = dlmread('data/hb_uv_clean.csv',';',1,0);
b = dlmread('data/hb_blue_clean.csv',';',1,0);
g = dlmread('data/hb_green_clean.csv',';',1,0);

% Read the file with human CIE data
% nm, CIE_A, CIE_D65, VM_l, V_l, x2, y2, z2, x10, y10, z10
cie = dlmread('data/all_1nm_data.csv',',',1,0);

% Define the wavelength span to use
w = 300:750;

white_xy = [0.3127 0.3290];

int_meth = 'PCHIP';

%% Honey bee sensitivity
% populate the honey bee sensitivity matrix
hb_sens = zeros( size(w,2), 3);

hb_sens(:,1) = interp1(u(:,1),u(:,2),w,int_meth,0);
hb_sens(:,2) = interp1(b(:,1),b(:,2),w,int_meth,0);
hb_sens(:,3) = interp1(g(:,1),g(:,2),w,int_meth,0);

% Plot the sensitivity function
tot_sens_hb = sum(hb_sens,2);
plot(w,tot_sens_hb)

%% Human sensitivity
% populate the human sensitivity matrix
human_sens = zeros( size(w,2), 3);

human_sens(:,1) = interp1(cie(:,1),cie(:,6),w,int_meth,0);
human_sens(:,2) = interp1(cie(:,1),cie(:,7),w,int_meth,0);
human_sens(:,3) = interp1(cie(:,1),cie(:,8),w,int_meth,0);

tot_sens_homo = sum(human_sens,2);
plot(w,tot_sens_homo)

%% How do the sensitivities differ?
sens_diff = tot_sens_homo - tot_sens_hb;
plot(w, sens_diff)

%% Create a led emittance distribution
% example led distribution 
%pdf('Normal',w,575,8)

% set up the matrices to hold the data
sens = cat(2, hb_sens, human_sens);
q = zeros(size(w,2), size(sens,2));
nr = 1;

% let a led light pass over the sensitivities
for i = w
    q(nr,:) = calc_stim_from_led(i,w,sens);
    nr = nr+1;
end

% q is now a smoothed sens matrix
q(q<0.0001) = 0; % remove noise to make the output cleaner

plot(w,q)

%% Calculate cie xy position and colour vector
xy_plane = zeros(size(w,2), 4);
xyz = repmat(sum(q(:,4:6),2),1,2);

xy_plane(:,1:2) = q(:,4:5) ./ xyz;

xy_angle = atan2(xy_plane(:,2)-white_xy(2), xy_plane(:,1)-white_xy(1)); %obs (y,x)
xy_magnitude = hypot(xy_plane(:,1)-white_xy(1), xy_plane(:,2)-white_xy(2));

% remove artefact from a
xy_angle(1:57) = NaN;

% plot the result
polar(xy_angle,xy_magnitude)

%% Calculate the complementary colour
% plot the hypothetical angle to the complementary colour
anti_angle = mod((xy_angle+2*pi),2*pi)-pi;
polar(anti_angle,xy_magnitude)

%% Find the index of the complementary colour
% Just iterate over the array and find the corresponding entry in the
% anti_a vector.

complementary_idx = zeros(size(w,2),1);
nr = 1;
for i = w
    [a,idx] = min( abs( xy_angle - anti_angle(nr) ) );
    % not more than 0.1 rad diff
    if(isnan(a) || abs( xy_angle(idx) - anti_angle(nr) ) > 0.1); 
        idx = NaN; 
    end;
    complementary_idx(nr) = idx;
    nr = nr + 1;
end

%complementary_w = w(complementary_idx);

%% Locate the best combinations
% Find the position with the smallest signal for a insect and largest 
% for a human 
nr = 1;
anti_q = zeros(size(w,2), size(sens,2));

for i = w
    if(~isnan( complementary_idx(nr) ))
        anti_q(nr,:) = calc_stim_from_led(w(complementary_idx(nr)),w,sens);
    end
    nr = nr+1;
end

tot_q = zeros(size(w,2),2);
tot_q(:,1) = sum(anti_q(:,1:3),2) + tot_sens_hb;
tot_q(:,2) = sum(anti_q(:,4:6),2) + tot_sens_homo;

tot_q(isnan(complementary_idx),:) = 0;
plot(w,tot_q)

%% what is the distance to white for each combo
% complementary_idx(~isnan(complementary_idx));
% w(~isnan(complementary_idx));
i = (1:size(w,2));
clean_com_idx = complementary_idx(~isnan(complementary_idx));
clean_idx = i(~isnan(complementary_idx));
clean_w = w(~isnan(complementary_idx));

scatter(xy_plane(clean_com_idx,1), xy_plane(clean_com_idx,2))

[x,y] = pol2cart(xy_angle, xy_magnitude);


%% Plot the difference b/w hb and human
optimal_light = tot_q(:,2) - tot_q(:,1);
plot(w,optimal_light)

%% Test the optimal light 
[m,l_max] = max(optimal_light);
% l-max led 1
l1 = w(l_max);
% l-max led 2
l2 = w(complementary_idx(l_max));
% Relativ intensity, output led 1 to 2
m1 = xy_magnitude(l_max);
m2 = xy_magnitude(complementary_idx(l_max));

l1
l2
m1
m2

%% Construct the stimulus
led_combo = normpdf(w,l1,8)/m1 + normpdf(w,l2,8)/m2;
led_array = repmat(led_combo', 1, 6);
stim = sens .* led_array;
receptorstim = sum(stim,1);
xyz = sum(receptorstim(:,4:6));
xyz_pos = receptorstim(:,4:6)/xyz 

% no of led2 for every 1 led of led1
m1/m2 %#ok<NOPTS>

plot(w,led_combo)


%% Plot the light in xy-space
plot(xy_plane(:,1), xy_plane(:,2))

