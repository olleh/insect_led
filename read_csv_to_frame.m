## Copyright (C) 2016 Olle Håstad
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} read_csv_to_frame (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Olle Håstad <olle@weasel.skallbe.net>
## Created: 2016-07-15

function clean_data = read_csv_to_frame (csv_filename, nm_frame)
csv_data = dlmread(csv_filename,';',1,0);
int_meth = 'PCHIP';
clean_data = interp1(csv_data(:,1),csv_data(:,2),nm_frame,int_meth,0);
endfunction
