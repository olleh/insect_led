## Copyright (C) 2016 Olle Håstad
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} plot_in_cie_white (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Olle Håstad <olle@weasel.skallbe.net>
## Created: 2016-07-17

function plot_in_cie_white (x_arr,y_arr)
x = [ 0.285
(0.382 - 0.05)/0.75
0.255 + 0.750 * 0.380
0.565
1.185-1.5*0.440
(0.440-0.15)/0.64
0.285
0.285
];

y = [ 0.05+0.75*x(1)
0.382
0.382
0.41
0.440
0.440
0.15+0.64*x(1)
0.05+0.75*x(1)
];

white_xy = [0.3127 0.3290];

load 'data/led_xy_plane.mat' led_xy_plane;

plot(led_xy_plane(:,1), led_xy_plane(:,2), 'LineWidth', 1)
hold on
line(x,y)
plot(x_arr,y_arr,'xr', 'MarkerSize', 10, 'LineWidth', 3)
plot(white_xy(1),white_xy(2),'.', 'LineWidth', 2)
hold off
endfunction
