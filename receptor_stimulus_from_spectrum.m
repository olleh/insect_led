## Copyright (C) 2016 Olle Håstad
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} receptor_stimulus_from_spectrum (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Olle Håstad <olle@weasel.skallbe.net>
## Created: 2016-07-15

function recept_stim = receptor_stimulus_from_spectrum (spec, sens)
spec_arr = repmat(spec', 1, size(sens,2));
stim = spec_arr .* sens;
recept_stim = sum(stim,1);
endfunction
